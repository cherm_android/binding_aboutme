package buu.chermkwan.aboutme

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.DataBindingUtil
import buu.chermkwan.aboutme.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val myName:MyName = MyName("Chermkwan")
    lateinit var myButton: Button
    lateinit var editText: EditText
    lateinit var nicknameTextView: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.apply {
            myButton = doneButton
            editText = nicknameEdit
            nicknameTextView = nicknameText

            myButton.setOnClickListener {v ->
                addNickname(v)
            }

            nicknameTextView.setOnClickListener {v ->
                updateNickname(v)
            }
            this.myName = this@MainActivity.myName
        }

    }

    private fun addNickname(v: View) {
        binding.apply {
            myName?.nickname = editText.text.toString()
            myButton.visibility = View.GONE
            editText.visibility = View.GONE
            nicknameTextView.visibility = View.VISIBLE
            val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(v.windowToken,0)
            invalidateAll()
        }

    }

    private fun updateNickname(v: View) {
        editText.visibility = View.VISIBLE
        myButton.visibility = View.VISIBLE
        v.visibility = View.GONE
        editText.requestFocus()
        val inputMethodManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.showSoftInput(editText,0)
    }

}
